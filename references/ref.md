﻿知識寶庫
==

## Spring技術
- [Spring Core](https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#beans)

&nbsp;

## Log
- [A Guide To Logback](https://www.baeldung.com/logback)
- [Java日志框架：logback详解](https://www.cnblogs.com/xrq730/p/8628945.html)
- [Log Level差異](https://zamhuang.medium.com/linux-%E5%A6%82%E4%BD%95%E5%8D%80%E5%88%86-log-level-216b975649a4)