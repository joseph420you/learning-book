﻿jasypy明文加密
==
> [!NOTE|label:Scenario|iconVisibility:hidden]
> 當密碼資訊被寫在**application.properties**(如下)，可能會導致資安問題，因此我們必需要對這類帶有重要資訊的明文進行加密，確保重要資訊不外流。

```java
spring.datasource.password=test%$%^passwaord
```

而<font color='red'>jasypt</font>可實現對機密資料的明文進行加密。

&nbsp;

## Step1. 在gradle內加入dependencies
```
implementation 'com.github.ulisesbocchio:jasypt-spring-boot-starter:3.0.2'
```

&nbsp;

## Step2. 在application.properties內加入：
```
jasypt.encryptor.password=incrte.com.igt
jasypt.encryptor.algorithm=PBEWithHmacSHA512AndAES_256
```
- 其中passwaord是加密私鑰。
- algorithm為加密演算法，若沒填，<font color='blue'>預設使用**PBEWithHmacSHA512AndAES_256**</font>。
- passwaord與algorithm可自行定義。

&nbsp;

## Step3. 取得密文
- 3.1 在Ccgeg Api尚未建立該功能前，是透過以下方式先取得密文
```java
@SpringBootApplication
public class CcgegApplication implements CommandLineRunner {
    @Autowired
    private ApplicationContext appCtx;

    @Autowired
    private StringEncryptor encryptor;
    
    public static void main(String[] args) {
        SpringApplication.run(CcgegApplication.class, args);
    }

    @Override
    public void run(String... args) {
        // 首先獲取配置文件里的原始明文信息
        Environment environment = appCtx.getBean(Environment.class); 
        String dbPassWord = environment.getProperty("spring.datasource.password");
        String cyphertext = encryptor.encrypt(dbPassWord);

        System.out.println("明文密碼：" + dbPassWord);
        System.out.println("加密後密文：" + cyphertext);
    }
}
```
dbPassWord取得**application.properties**內的`spring.datasource.password`密碼test%$%^passwaord，並對其加密，結果會印在Tomcat的console內。

&nbsp;

- 3.2 Ccgeg Api已提供該功能，可使用[Swagger](maintenance\CHANGE_PASSWORD.md)取得密文。

&nbsp;

## Step4. 修改application.properties
假設加密後的密文為 /xUAa7/XKP03Y1koLdAUmZTEstjjijo。
```
spring.datasource.password=ENC(/xUAa7/XKP03Y1koLdAUmZTEstjjijo)

jasypt.encryptor.password=incrte.com.igt
jasypt.encryptor.algorithm=PBEWithHmacSHA512AndAES_256
```
&emsp;基本上，到此步驟，已算是把資料庫密碼加密起來，但此時的<font color='red'>私鑰和加密演算法仍曝露在設定檔</font>內，隨著**application.properties**執行push上到gitlab後，仍容易找到此資訊。因此在下一個步驟將對`jasypt.encryptor.password`與`jasypt.encryptor.algorithm`等資訊隱藏在java code內。

&nbsp;

## Step5. 隱藏私鑰與加密演算法資訊
Ccgeg Api採用@Bean方式進行資訊隱藏。
```java
@Configuration
public class EncryptorConfig {
    @Primary
    @Bean(name = "stringEncryptor")
    public StringEncryptor getEncryptor() {
        PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
        encryptor.setConfig(getConfig());

        return encryptor;
    }

    private SimpleStringPBEConfig getConfig() {
        SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setPassword("incrte.com.igt");
        config.setAlgorithm("PBEWithHmacSHA512AndAES_256");
        config.setKeyObtentionIterations("1000");
        config.setPoolSize("1");
        config.setProviderName("SunJCE");
        config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
        config.setIvGeneratorClassName("org.jasypt.iv.RandomIvGenerator");
        config.setStringOutputType("base64");

        return config;
    }
}
```
- 自定義的Bean要賦予名字，以便後續在application.properties內指定。

> [!DANGER|label:Notice|iconVisibility:hidden]
> 因為在專案內部有提供取得密文之api功能(EncryptService.java)，內有`@Auotowired StringEncryptor`，而Spring IoC容器會進行Bean的初始，但這裡自定義取得StringEncryptor的方式會與Spring內取得StringEncryptor方式相衝，IoC會不知道要使用哪一個方式來取得Bean，會產生 

>> Consider marking one of the beans as @Primary, updating the consumer to accept multiple beans, or using @Qualifier to identify the bean that should be consumed

> 因此要加上[@Primary](https://www.baeldung.com/spring-primary)告知Spring IoC，採用自定義方式取得StringEncryptor的Bean。

&nbsp;

最後，移除**application.properties**內的私鑰與加密演算法資訊，並加上自定義加密器Bean名字，便完成功能。
```
spring.datasource.password=ENC(/xUAa7/XKP03Y1koLdAUmZTEstjjijo)
jasypt.encryptor.bean=stringEncryptor
```