﻿Bean的生命週期
==

![PNG](..\images\spring\beancycle.png)
<center>圖一 Bean生命週期圖</center>

&nbsp;

初始化(Initializing)
===

當Bean進行實體化(Instantiating)，即建構子執行完畢，並完成屬性注入後，若Bean有實作`BeanNameAware`、`BeanFactoryAware`或`ApplicationContextAware`時，將依序執行這些實作介面後的方法 : 
- `setBeanName(name: String)`
- `setBeanFactory(beanFactory: BeanFactory)`
- `setApplicationContext(applicationContext: ApplicationContext)`

&nbsp;

而Bean的初始化有3種方式實現：
- `@PostConstant`註解在method上。
- class實作`InitializingBean`的`afterPropertiesSet()`。
- 透過指定`init-method`:
	- xml：`<bean id="myBean" class="com.demo.MyBean" init-method="init">`
	- java annotation：`@Bean(initMethod = "init")`

&nbsp;

若希望在Bean實體化後，<font color='red'>初始化執行前</font>，進行一些其他配置或增加額外的處理邏輯，則可額外新增一個Bean，並且覆寫`BeanPostProcessor`的`postProcessBeforeInitialization()`即可。該實作Bean，會對所有自定義的Bean進行處理。而`postProcessBeforeInitialization()`將會被`AbstractAutowireCapableBeanFactory`內的`applyBeanPostProcessorsBeforeInitialization()`進行呼叫。

&nbsp;

在初始化執行完畢後，希望在<font color='red'>開始對bean進行操作前</font>，額外新增初始化的後續處理，則可在有實作`BeanPostProcessor`內，覆寫`postProcessAfterInitialization()`。

> [!WARNING|label:WARNING]
> `BeanPostProcessor`的`postProcessBeforeInitialization()`與`postProcessBeforeInitialization()`所回傳的物件<font color='red'>不得為null</font>。

&nbsp;

在完成以上階段後的Bean，可開始執行自定義的函式操作。

&nbsp;

銷毀(Destroying)
===
當容器被通知關閉時，若希望Bean能在容器關閉前，執行Bean銷毀前的資源釋放或其他處理時，可進行以下的銷毀實作：
- `@PreDestroy`註解在method上。
- class實作`DisposableBean`的`destroy()`。
- 透過指定`destroy-method`:
	- xml：`<bean id="myBean" class="com.demo.MyBean" destroy-method="destroy">`
	- java annotation：`@Bean(destroyMethod = "destroy")`

&nbsp;

程式碼範例
===
```java
public class Bean_1 implements
        BeanNameAware,
        BeanFactoryAware,
        ApplicationContextAware,
        InitializingBean,
        DisposableBean {

    public Bean_1(){
        System.out.println("Bean_1.construct completes");
    }

    @Override
    public void setBeanName(String name) {
        System.out.println("Bean_1.setBeanName()");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("Bean_1.setBeanFactory()");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("Bean_1.setApplicationContext()");
    }

    @PostConstruct
    public void postConstruct(){
        System.out.println("Bean_1.@PostConstruct");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("Bean_1.InitializingBean.afterPropertiesSet()");
    }

    public void initMethod(){
        System.out.println("Bean_1.init-method");
    }

    public void hello(){
        System.out.println("Bean_1.hello()");
    }

    @PreDestroy
    public void preDestroy(){
        System.out.println("Bean_1.@PreDestroy");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("Bean_1.DisposableBean.destroy()");
    }

    public void destroyMethod(){
        System.out.println("Bean_1.destroy-method");
    }
}
```
定義一個class，而這僅僅只是一個class。

&nbsp;

```java
@Configuration
public class CustomConfig {

    //這僅僅是為了演示所定義的建構子，實務上不需要
    public CustomConfig(){
        System.out.println("CustomConfig.construct completes");
    }

    @Bean(initMethod = "initMethod", destroyMethod = "destroyMethod")
    public Bean_1 bean_1(){
        return new Bean_1();
    }
}
```
`@Configuration`告知IoC，此為一個配置類，並將剛剛的`Bean_1`類別在此配置類內定義為一個Spring Bean。

&nbsp;

```java
@Component
public class BeanPostProcessorImpl implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("BeanPostProcessor.postProcessBeforeInitialization()處理" + beanName);
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("BeanPostProcessor.postProcessAfterInitialization()處理" + beanName);
        return bean;
    }
}
```
定義一個`BeanPostProcessor`，待app啟動後，會<font color='red'>對所有自定義的Spring Bean</font>進行初始化過程中的前置與後置處理。

&nbsp;

```java
public static void main(String[] args) {
	AnnotationConfigApplicationContext context = 
			new AnnotationConfigApplicationContext(
					BeanPostProcessorImpl.class, 
					CustomConfig.class
			);

	Bean_1 o = context.getBean(Bean_1.class);
	o.hello();

	context.close(); //容器關閉
}
```
```java
(...省略...)
14:57:32.779 [main] DEBUG org.springframework.beans.factory.support.DefaultListableBeanFactory - Creating shared instance of singleton bean 'beanPostProcessorImpl'
14:57:32.795 [main] DEBUG org.springframework.beans.factory.support.DefaultListableBeanFactory - Creating shared instance of singleton bean 'customConfig'
CustomConfig.construct completes
BeanPostProcessor.postProcessBeforeInitialization()處理customConfig
BeanPostProcessor.postProcessAfterInitialization()處理customConfig
14:57:32.797 [main] DEBUG org.springframework.beans.factory.support.DefaultListableBeanFactory - Creating shared instance of singleton bean 'bean_1'
Bean_1.construct completes
Bean_1.setBeanName()
Bean_1.setBeanFactory()
Bean_1.setApplicationContext()
BeanPostProcessor.postProcessBeforeInitialization()處理bean_1
Bean_1.@PostConstruct
Bean_1.InitializingBean.afterPropertiesSet()
Bean_1.init-method
BeanPostProcessor.postProcessAfterInitialization()處理bean_1
(...省略...)
Bean_1.hello()
(...省略...)
Bean_1.@PreDestroy
Bean_1.DisposableBean.destroy()
Bean_1.destroy-method
```
從執行結果可以看到：
- `DefaultListableBeanFactory`是Spring內可獨立運作的IoC。
- 在`CustomConfig`完成物件實體化(instantiating)後，`BeanPostProcessor`會對`CustomConfig`進行前置與後置處理。
- 根據`@Configuration`(這裡為`CustomConfig`)開始對Spring Bean(這裡為`Bean_1`)進行實體化(instantiating)與初始化(initializing)。
- 而`Bean_1`一樣在實體化(instantiating)後，交由`BeanPostProcessor`進行前置與後置處理。
- 整個Spring Bean的輸出結果可以比照最一開始的Bean生命週期圖。

&nbsp;

備註：
- `org.springframework.beans.factory.support.DefaultListableBeanFactory - Creating shared instance of singleton bean...(省略)`僅僅為啟動過程中的log，不表示當下已完成Spring Bean的實體化。
- 真正實體化的時間點請參照結果訊息為`XXX.construct completes`為主。
