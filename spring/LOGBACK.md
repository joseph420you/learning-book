﻿Logback日誌管理
==
> [!NOTE|label:Scenario|iconVisibility:hidden]
> 一個好的日誌系統對問題回溯有很大的幫助，而我們採用logback日誌主要為logback是Spring Boot默認使用日誌管理，內含在`spring-boot-starter-web`內了，且提供對不同日誌級別歸類到不同文件。

&nbsp;

## Log範例格式
```
2020-12-16 16:14:03.177 [main] WARN  org.springframework.boot.autoconfigure.orm.jpa.JpaBaseConfiguration$JpaWebConfiguration - spring.jpa.open-in-view is enabled by default. Therefore, database queries may be performed during view rendering. Explicitly configure spring.jpa.open-in-view to disable this warning
2020-12-16 16:14:03.652 [main] INFO  org.apache.coyote.http11.Http11NioProtocol - Starting ProtocolHandler ["http-nio-8080"]
2020-12-16 16:14:03.678 [main] INFO  org.springframework.boot.web.embedded.tomcat.TomcatWebServer - Tomcat started on port(s): 8080 (http) with context path ''
2020-12-16 16:14:03.876 [main] INFO  org.springframework.data.repository.config.DeferredRepositoryInitializationListener - Triggering deferred initialization of Spring Data repositories…
2020-12-16 16:14:04.195 [main] INFO  org.springframework.data.repository.config.DeferredRepositoryInitializationListener - Spring Data repositories initialized!
```
首先，先截取一段Tomcat內的log來看。從內容可看到一段log由以下資訊組成：
- 時間日期：精確到毫秒
- 執行緒名稱：e.g. [main]
- 日誌级别：TRACE, DEBUG, INFO, WARN, ERROR (低→高)
- Logger名稱：通常使用類別名
- 分隔符號：\- 後為日誌資訊 

&nbsp;

> [!NOTE|label:Logback Architecture|style:flat]
> Logback架構主要為Logger, Appender和Layout。

&nbsp;

## 配置設定
於`\resources`內新增</font color='red'>logback-spring.xml</font>。以下為一個logback-spring.xml範本，於下一節一一說明。

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<configuration>

    <property name="LOG_PATTERN" value="%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger - %msg%n"/>

    <springProperty scope="context" name="APP_NAME" source="spring.application.name"/>
    <springProperty name="LOG_FILE" source="logging.file.path"/>

    <!--輸入到console-->
    <appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
        <layout class="ch.qos.logback.classic.PatternLayout">
            <pattern>${LOG_PATTERN}</pattern>
        </layout>
    </appender>

    <!--輸出到文件-->
    <appender name="ROLLING-FILE" class="ch.qos.logback.core.FileAppender">
    	<file>D:/123.log</file>  
		<append>true</append>
        <encoder>
            <pattern>${LOG_PATTERN}</pattern>
        </encoder>
    </appender>

    <logger name="java" level="INFO">
        <appender-ref ref="CONSOLE" />
    </logger>

    <logger name="java.lang.Integer" level="WARN">
        <appender-ref ref="ROLLING-FILE" />
    </logger>
    
    <springProfile name="dev">
        <root level="info">
            <appender-ref ref="CONSOLE" />
        </root>
    </springProfile>    
</configuration>
```
> [!TIP|label:configuration]
> - 配置logback。

<br>

> [!TIP|label:property]
> - 自定義變數。
> - name屬性為該property名稱。
> - value屬性用來儲存值。
> - 若要使用該變數，則可以%{變數名稱}來使用。

<br>

> [!TIP|label:springProfile]
> - `<springProfile name=“dev”>`的name屬性，其值會對應到spring配置檔內的`spring.profiles.active`。
> - 以上面配置檔為例，表示在application.properties內亦該存在一筆設定`spring.profiles.active=dev`。

<br>

> [!TIP|label:springProperty]
> - 用來讀取Spring配置檔(application.properties/application.yml)並存放其設定值。
> - name屬性為該springProperty名稱。
> - source屬性為Spring配置黨內的key，例：`spring.application.name`(application.properties)。

&nbsp;

> [!TIP|label:appender]
> - 日誌的輸出策略，例如日誌資訊格式。
> - name屬性為該appender名稱。
> - class屬性用來指定何種策略。
>	- `ch.qos.logback.core.ConsoleAppender`：輸出log到Console。
>	- `ch.qos.logback.core.FileAppender`：輸出log到文件。
>	- `ch.qos.logback.core.rolling.RollingFileAppender`：依條件輸出log到不同文件，於下面詳細說明。
>	- `ch.qos.logback.classic.AsyncAppender`：異步輸出log到文件，於下面詳細說明。
> - `<filter>`可輸出指定log級別以上的輸出。
> - `<encoder>`用來定義log訊息的格式，於0.9.19版本之後引入，早些版本使用<layout>，logback極力推薦的是使用<encoder>而不是<layout>。
> - `<file>`為寫入的檔名，可使用相對目錄或絕對目錄，若上層目錄不存在則自動創建。
> - `<append>`true則表示log直接續加在文件底部；false則先清空文件。
> <br>
> <br>
> ##### RollingFileAppender
> - RollingFileAppender的作用是將log記錄到指定文件，當符合某個條件時再將日誌記錄到其他文件，其配置比較靈活，因此使用得更多。
> ```xml
>   <appender name="ROLLING-FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
>       <encoder>
>           <pattern>${LOG_PATTERN}</pattern>
>       </encoder>
>       <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
>           <fileNamePattern>${LOG_FILE}.%d{yyyy-MM-dd}.log</fileNamePattern>
>           <maxHistory>30</maxHistory>
>       </rollingPolicy>
>   </appender>
> ```
>   - `<rollingPolicy>`指定rolling規則，範例採用常見依據時間進行rolling的`TimeBasedRollingPolicy`。
>   - `<fileNamePattern>`為必要節點。<font color='red'>%d</font>可以包含一個`java.text.SimpleDateFormat`指定的時間格式，如%d{yyyy-MM}，若直接使用%d，預設為yyyy-MM-dd。
>   - RollingFileAppender內的<file>可有可無，但通過設置<file>可為活動文件和歸檔文件指定不同的位置。
>   - `<maxHistory>`為optional，設定保留歸檔文件數量，若超過則刪除，單位為月。
> <br>
> <br>
> ##### 異步日誌
> - log通常都會寫入磁碟，例如使用RollingFileAppender，而每寫一次log便會產生一次IO，對性能有極大的影響，採用異步日誌可減少IO次數。
> ```xml
>   <appender name="ASYNC-FILE" class="ch.qos.logback.classic.AsyncAppender">
>       <discardingThreshold>0</discardingThreshold>
>       <queueSize>256</queueSize>
>       <appender-ref ref="ROLLING-FILE"/>
>   </appender>
> ```
>   - `<discardingThreshold>`假如等於20則表示，表示當還剩20%容量時，將丟棄TRACE, DEBUG與INFO的Event，只保留WARN與ERROR的Event，為了保留所有的events，可以將這個值設置為0，默認值為queueSize/5。
>   - `<queueSize>`容量預設值為256。
>   - `<appender-ref>`表示AsyncAppender使用哪個appender進行log輸出。

&nbsp;

> [!TIP|label:logger]
> - 程式碼內的`LoggerFactory.getLogger(String : name)`，參數name會到logback-spring.xml內找符合其name的<logger>。
> - name通常會使用類別名(package)，或自行命名亦可。
> - 若找不到符合的<logger>，則無作用；但若有定義<root>，則會使用此logger。
> - root為所有logger的父級別。
> ```xml
>	<logger name="AA" level="WARN">
>       <appender-ref ref="CONSOLE"/>
>	</logger>
>
>	<root level="INFO">
>		<appender-ref ref="CONSOLE" />
>	</root>
> ```
> 子logger 'AA'的level會覆寫root的level，因此只會印出WARN和ERROR訊息。
> <br>
> <br>
> - 若使用類別命名，要注意繼承關係會影響結果。
> ```xml
>   <logger name="java.lang" level="WARN">
>       <appender-ref ref="CONSOLE" />
>   </logger>
> ```
>   - 若此時代碼內為`LoggerFactory.getLogger(Double.class)`，則會順利找到`java.lang`logger，因為Double位於`java.lang`底下。
>   - 反之，若為`LoggerFactory.getLogger(System.class)`，則會因找不到該logger而無作用。

&nbsp;

更多詳細參考資料可於本書知識寶庫內的[Log](..\references\ref.md#Log)分類觀看。