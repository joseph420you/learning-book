﻿密碼修改
==
> [!NOTE|label:Scenario|iconVisibility:hidden]
> 如果哪天，經過[jasypt](..\systemdesign\ENCRYPTOR.md)加密後的密碼如果受到變更，例如資料庫密碼，那我們該如何修改**application.properties**內受到ENC(*)保護的密文呢?

&nbsp;

## 使用Ccgeg Api Swagger-UI
> [!TIP|style:flat|label:Steps]
> 1. 進到<font color='red'>ccgeg-api/swagger-ui/</font>
> 2. 右上角Select a definition選擇<font color='blue'>Internal Services</font>
> 3. 點擊服務<font color='blue'>jasypt資料加密服務</font>
> 4. 點開<font color='blue'>/ccgeg-api/encrypt</font>
> 5. 右邊點開<font color='blue'>Try it out</font>
> 6. 於範例的Request body下方{"text": "string"}，將string內容替換成新密碼後按下<font color='blue'>Execute</font>(如圖一)
> 7. 拉至下方Response區，若成功，則會在Code 200的Response body看到包覆<font color='blue'>"cyphertext": "密文亂碼"</font>類似這樣的JSON資料(如圖二)
> 8. 複製<font color='blue'>密文亂碼</font>
> 9. 替換**application.properties**的<font color='blue'>ENC括號內的密文</font>，即完成密碼更新

&nbsp;

![PNG](..\images\maintenance\CHANGE_PASSWORD\D1.png)
<center>圖一 對密碼進行加密</center>

&nbsp;

![PNG](..\images\maintenance\CHANGE_PASSWORD\D2.png)
<center>圖二 取得密文</center>