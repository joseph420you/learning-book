* Spring
	- [Bean生命週期](spring\BEAN_LIFE_CYCLE.md)
	- [Logback日誌管理](spring\LOGBACK.md)
	- [jasypt明文加密](spring\ENCRYPTOR.md)
	- [例外處理](spring\EXCEPTION.md)
	- [測試架構](spring\TESTING.md)

* 軟體設計
	- [使用Enum替換多個if/else邏輯條件敘述](softwaredesign\ENUM_IFELSE.md)
	- [Proxy Pattern](softwaredesign\PROXY.md)	
	- [JDK動態代理](softwaredesign\JDK_PROXY.md)
	- [深入JDK動態代理例外問題探討](softwaredesign\JDK_PROXY_ADV.md)

* 維運
	- [密碼修改](maintenance\CHANGE_PASSWORD.md)
	
* Q&A
	- [測試大哉問](questions\Q1.md)

* [知識寶庫](references\ref.md)