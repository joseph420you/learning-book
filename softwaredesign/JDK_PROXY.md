﻿JDK動態代理
==
> [!NOTE|label:Scenario|iconVisibility:hidden]
> 暨上一篇[Proxy Pattern](PROXY.md)對於代理有基本了解後，進一步來探討Java提供的代理模式。

動態代理是很多框架和技術的基礎，<font color='red'>Spring AOP就是基於動態代理實現的</font>。了解動態代理的機制對於理解AOP的底層實現是很有幫助的。

&nbsp;

以Proxy Pattern章節的[例子](PROXY.md#範例情境)來繼續，我們可以去掉`ProxyLogin`這個代理類，轉而實作`java.lang.reflect.InvocationHandler`來達到動態代理。

直接來看以下範例程式：

```java
public class LoginHandler implements InvocationHandler {
    //要代理的真實對象
    private Object delegate;
    
    public Object bind(Object delegate) {
        this.delegate = delegate;
        return Proxy.newProxyInstance(
                delegate.getClass().getClassLoader(),
                delegate.getClass().getInterfaces(),
                this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("驗證會員中...");
        return method.invoke(delegate, args);
    }
}
```

&nbsp;

而測試代碼就會變成如下：
```java
public static void main(String[] args) {
	LoginHandler handler = new LoginHandler();
	ILogin obj = (ILogin)handler.bind(new UserLogin("Joseph"));
	obj.login();
}
```

> [!TIP|label:java.lang.reflect.InvocationHandler]
> - 實作InvocationHandler介面建立自己的Handler。
> - 覆寫`Object invoke(Object proxy, Method method, Object[] args)`
>	- proxy為代理實例。
>	- method為被攔截的方法，此例會是login()。
>	- args為該方法的參數列，此例若去迭代args會得到null，因login無參數帶入。
> - `method.invoke`利用反射機制將請求分派給委託類處理。`invoke()`返回Object物件作為方法執行結果。 
> - 在`invoke()`內可以加入任何邏輯，例如修改方法引數、加入日誌功能、安全檢查功能等。

&nbsp;

> [!TIP|label:java.lang.reflect.Proxy]
> - 撰寫`Proxy.newProxyInstance()`產生代理物件代碼。
> - `public static Object newProxyInstance(ClassLoader loader, Class<?>[] interfaces, InvocationHandler h) throws IllegalArgumentException`
>	- 回傳一個實作指定介面的代理物件，對該物件的所有方法呼叫都會轉發給`InvocationHandler.invoke()`方法。
>	- `loader`加載代理對象。
>	- `interfaces`為執行時期動態產生的代理物件欲實作的介面陣列集合(因可能實際業務物件本身有實作多個介面)，由於業務物件`UserLoging`實作的是`ILogin`，因此生成的代理物件也會跟著實作`ILogin`。
>	- `h`指定欲轉發方法執行的Handler對象。

&nbsp;

> [!DANGER|label:Notice|iconVisibility:hidden]
> - 通過`Proxy.newProxyInstance`創建的<font color='red'>代理物件為JVM執行時期動態生成</font>的，它並不是我們的InvocationHandler類型，也不是我們定義的那組介面的類型。
> - 命名方式以$開頭，Proxy為中，最後一個數字表示對象的標號，例：$Proxy0。
> - 所有的JDK動態代理都會繼承`java.lang.reflect.Proxy`。
> - $Proxy0物件相當於[Proxy Pattern](PROXY.md)章節內的範例[ProxyLogin](PROXY.md#範例情境)物件。
> - <font color='red'>JDK給目標類提供動態要求目標類必須實作介面</font>(範例中以自定義的`ILogin`介面為例)，<u>當一個目標類不實作介面時，JDK是無法為其提供動態代理的，但<font color='#00A600'>cglib</font> 卻能給這樣的類提供動態代理</u>。

&nbsp;

> [!WARNING|label:Error]
> 注意！若此時範例內產生代理物件後做的強制轉型誤寫成子型別`UserLogin`，<font color='red'>執行時期</font>會發生<font color='red'>java.lang.ClassCastException: com.sun.proxy.$Proxy0 cannot be cast to UserLogin</font>。

> 例：`UserLogin obj = (UserLogin)handler.bind(new UserLogin("Joseph"));`
> <br>
> <br>
> 原因是因為`Proxy.newProxyInstance()`的第二個參數`interfaces`已經指名欲生成的代理物件要進行實作的介面型別為`ILogin`，而非類別`UserLogin`，因此發生轉型失敗。
> <br>
> <br>
> 也因為代理模式本身設計的關係，`Proxy.newProxyInstance()`第二個參數只能接收介面型別的`Class<?>[]`，而官方<u>API文件在此方法的參數名上也明確命名為interfaces，所以請別誤傳入類別型別</u>。
> <br>
> <br>
> 若要驗證上述觀點，可自行新增一個介面`ITest`，且不修改原本`UserLogin`的實作關係，但修改`LoginHandler`如下：
> ```java
> return Proxy.newProxyInstance(
>		delegate.getClass().getClassLoader(),
>		new Class<?>[]{ITest.class},
>		this);
> ```
> 
> `main()`內修改成`ITest obj = (ITest)handler.bind(new UserLogin("Joseph"));`，<font color='#00A600'>執行時期會通過</font>(在此先不論接下來的業務邏輯功能性)。