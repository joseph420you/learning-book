﻿深入JDK動態代理UndeclaredThrowableException探討
==
> [!NOTE|label:Scenario|iconVisibility:hidden]
> 暨上一篇[JDK動態代理](JDK_PROXY.md)對於JDK動態代理有基本了解後，進一步來探討一旦委託物件的方法拋出了checked例外，會產生甚麼樣的問題點?

&nbsp;

以下針對Proxy Pattern章節的[例子](PROXY.md#範例情境)，新增一個自定義的checked例外在`login()`內，而介面也一併新增例外宣告如下：

```java
public interface ILogin {
    void login() throws AccountNotFoundException;
}
```

&nbsp;

`UserLogin.java`內修改`login()`如下：
```java
@Override
public void login() throws AccountNotFoundException {
	if(!userName.equals("joseph")){
		throw new AccountNotFoundException();
	}

	System.out.println(userName+"登入");
}
```

&nbsp;

而測試程式修改登入的會員名稱為KiKi如下：
```java
public static void main(String[] args) {
    LoginHandler handler = new LoginHandler();
    ILogin obj = (ILogin)handler.bind(new UserLogin("KiKi"));
    obj.login();
}
```

&nbsp;

運行後的結果如下：
```java
Exception in thread "main" java.lang.reflect.UndeclaredThrowableException
	at com.sun.proxy.$Proxy0.login(Unknown Source)
	at com.example.MySpring.proxy.test.main(test.java:15)
Caused by: java.lang.reflect.InvocationTargetException
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at com.example.MySpring.proxy.LoginHandler.invoke(LoginHandler.java:25)
	... 2 more
Caused by: com.example.MySpring.proxy.AccountNotFoundException
	at com.example.MySpring.proxy.UserLogin.login(UserLogin.java:17)
	... 7 more
驗證會員中...
```

&nbsp;

> [!DANGER|label:Notice|iconVisibility:hidden]
> 由例外結果明顯看到我們原本預期會拋出的例外應該只有`AccountNotFoundException`，但實際上卻又多包了兩層，分別為`java.lang.reflect.InvocationTargetException`與`java.lang.reflect.UndeclaredThrowableException`。

&nbsp;

## 問題說明
根據上一節[JDK動態代理](JDK_PROXY.md)內提到，在執行時期JVM動態生成代理物件，其命名方式會類似`$ProxyN`(N為數字)。在例外StackTrace內有清楚說明最後的`UndeclaredThrowableException`是由`com.sun.proxy.$Proxy0.login`噴出，接下來我們就來一窺`$Proxy0`內到底長甚麼樣子。

&nbsp;

以下代碼可產生`$Proxy0`位元碼：
```java
final String CLASS_NAME = "$Proxy0";
byte[] data = ProxyGenerator.generateProxyClass(CLASS_NAME, new Class[]{ILogin.class});

FileOutputStream out = new FileOutputStream("C:\\Users\\user\\Desktop\\" + CLASS_NAME + ".class");
out.write(data);
out.close();
```
利用`Proxy.generateProxyClass(String var0, Class<?>[] var1)`來取得位元碼，透過產生的位元碼資料再寫進到`.class`內，最後將產生的位元碼檔透過Intellij反解譯成java code。
- `var0`決定該類別的名稱，可隨意取。
- `var1`傳入代理物件所實作的介面型別，這裡為`ILogin`。

&nbsp;

產生的`.class`內容經過IDE反解譯後的內容如下：
```java
import com.example.MySpring.proxy.AccountNotFoundException;
import com.example.MySpring.proxy.ILogin;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.UndeclaredThrowableException;

public final class $Proxy0 extends Proxy implements ILogin {
    private static Method m1;
    private static Method m2;
    private static Method m3;
    private static Method m0;

    public $Proxy0(InvocationHandler var1) throws  {
        super(var1);
    }

    public final boolean equals(Object var1) throws  {
        try {
            return (Boolean)super.h.invoke(this, m1, new Object[]{var1});
        } catch (RuntimeException | Error var3) {
            throw var3;
        } catch (Throwable var4) {
            throw new UndeclaredThrowableException(var4);
        }
    }

    public final String toString() throws  {
        try {
            return (String)super.h.invoke(this, m2, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

    public final void login() throws AccountNotFoundException {
        try {
            super.h.invoke(this, m3, (Object[])null);
        } catch (RuntimeException | AccountNotFoundException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

    public final int hashCode() throws  {
        try {
            return (Integer)super.h.invoke(this, m0, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

    static {
        try {
            m1 = Class.forName("java.lang.Object").getMethod("equals", Class.forName("java.lang.Object"));
            m2 = Class.forName("java.lang.Object").getMethod("toString");
            m3 = Class.forName("com.example.MySpring.proxy.ILogin").getMethod("login");
            m0 = Class.forName("java.lang.Object").getMethod("hashCode");
        } catch (NoSuchMethodException var2) {
            throw new NoSuchMethodError(var2.getMessage());
        } catch (ClassNotFoundException var3) {
            throw new NoClassDefFoundError(var3.getMessage());
        }
    }
}
```

&nbsp;

首先，我們可以看到其建構子接收`InvocationHandler`參數，其指定的Hanlder便是在[JDK動態代理](JDK_PROXY.md)內的`LoginHandler.java`，以下直接擷取代碼片段：
```java
public class LoginHandler implements InvocationHandler {
    //省略

    public Object bind(Object delegate) {
        this.delegate = delegate;
        return Proxy.newProxyInstance(
                delegate.getClass().getClassLoader(),
                delegate.getClass().getInterfaces(),
                this);
    }

    //省略
}
```
於`newProxyInstance()`內第三個參數直接指定`this`。

&nbsp;

接下來，在`$Proxy0.login()`內明明有看到`catch(AccountNotFoundException)`，究竟出了甚麼問題? 


原因出在`h.invoke(this, m3, (Object[])null);`，白話說，即為`LoginHandler.invoke(Object proxy, Method method, Object[] args) throws Throwable`，內部`method.invoke()`會呼叫委託類實際方法。

若我們再直接點擊`method.invoke()`進去看，其API文件內對`java.lang.reflect.InvocationTargetException`說明如下：
> if the underlying method throws an exception.
> (若底層方法拋出例外)

&nbsp;

因此，答案呼之欲出！

&nbsp;

一旦委託類的`login()`發生`AccountNotFoundException`，經由`method.invoke()`會包一層`InvocationTargetException`，於`LoginHandler.invoke()`內再往上丟到`$Proxy0.login()`，會被`catch (Throwable var3)`捕捉到，最後再包成`UndeclaredThrowableException`，最終變成我們看到的結果。

&nbsp;

> [!TIP|label:Solution]
> 要如何只呈現我們想要的預期例外結果，只拋出`AccountNotFoundException`?
> 只要在`LoginHandler.invoke()`新增捕捉`InvocationTargetException`，改拋出原始例外即可，如下：
> ```java
> @Override
> public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
>	System.out.println("驗證會員中...");
>	try {
>		return method.invoke(delegate, args);
>	}catch(InvocationTargetException e){
>		throw e.getCause();
>	}
> }
> ```
> <br>
> <br>
> 運行結果如下：
> ```java
> 驗證會員中...
> Exception in thread "main" com.example.MySpring.proxy.AccountNotFoundException
>	at com.example.MySpring.proxy.UserLogin.login(UserLogin.java:17)
>	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
>	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
>	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
>	at java.lang.reflect.Method.invoke(Method.java:498)
>	at com.example.MySpring.proxy.LoginHandler.invoke(LoginHandler.java:27)
>	at com.sun.proxy.$Proxy0.login(Unknown Source)
>	at com.example.MySpring.proxy.test.main(test.java:16)
> ```