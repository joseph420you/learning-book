﻿Proxy Pattern
==
> [!NOTE|label:Scenario|iconVisibility:hidden]
> 有時維護時，若需要對某些核心重要代碼多加檢查機制或是附加功能，要如何達到S.O.L.I.D.的OCP(Open-Closed Principle)呢?

&nbsp;

代理模式(Proxy Pattern)是一種常用的設計模式，目的是提供一個代理來控制對實際物件的操作，代理類內部在委託實際物件進行操作前，會執行自己的附加功能，例如印出日誌。
 
![PNG](..\images\softwaredesign\PROXY\D1.png)
<center>圖一 Proxy Pattern UML圖</center>

&nbsp;

由上圖得知，為了保持行為一致性，Proxy和RealObject會實作同一個介面，通過Proxy這中間一層，能有效控制對RealObject的直接訪問，能很好地隱藏和保護委託物件，而Proxy最終仍要委託RealObject的方法來執行。

&nbsp;

## 範例情境
> [!NOTE|label:Scenario|iconVisibility:hidden]
> 希望針對會員登入功能，新增驗證會員資格功能，並且不異動既有登入程式。

代碼如下：
1. 定義登入介面。
```java
public interface ILogin {
    void login();
}
```
<br>
2. 定義RealObject實作。
```java
public class UserLogin implements ILogin {
    private String userName;

    public UserLogin(String userName) {
        this.userName = userName;
    }

    @Override
    public void login() {
        System.out.println("會員 [" + userName + "] 已登入");
    }
}
```
<br>
3. 定義Proxy物件。
```java
public class ProxyLogin implements ILogin {
    private ILogin loginLogic;

    public ProxyLogin(ILogin loginLogic){
        this.loginLogic = loginLogic;
    }

    @Override
    public void login() {
        System.out.println("驗證會員中...");
        loginLogic.login();
    }
}
```
<br>
4. 測試main()。
```java
public static void main(String[] args) {
    String userName = "Joseph";

    System.out.println("--- 未加入代理驗證前的登入流程 ---");
    UserLogin login = new UserLogin(userName);
    login.login();

    System.out.println();
    
    System.out.println("--- 加入代理驗證後的登入流程 ---");
    ProxyLogin proxyLogin = new ProxyLogin(new UserLogin(userName));
    proxyLogin.login();
}
```
<br>
5. 運行結果。
```
--- 未加入代理驗證前的登入流程 ---
會員 [Joseph] 已登入
--- 加入代理驗證後的登入流程 ---
驗證會員中...
會員 [Joseph] 已登入
```


- 因為代理類由程式碼撰寫，在<font color='blue'>編譯時期便已決定代理類和委託類間的關係</font>，並且會產生.class檔，因此這樣的代理模式又稱為<font color='red'>靜態代理</font>。
- 另一種動態代理為[JDK動態代理](JDK_PROXY.md)，為<font color='blue'>執行時期利用反射機制動態建立</font>。
- 代理最大的優點，業務類只需要關注業務邏輯本身，保證了業務類的重用性。


> [!TIP|style:flat|label:Tip]
> - 因為一個代理物件只為一個委託物件服務，一旦需要進行代理的Method變多，則代理類會爆增。

> - 由於代理和委託實作同一個介面，若介面新增了method，則所有子類別均受影響，大幅增加程式碼維護性。

> - [JDK動態代理](JDK_PROXY.md)可解決上述問題，適用於較大規模程式架構，且靈活性較高。

> - 靜態代理實作簡單，適合代理類較少且確定的情況。