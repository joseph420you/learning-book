﻿使用Enum替換多個if/else邏輯條件敘述
==
> [!NOTE|label:Scenario|iconVisibility:hidden]
> 當程式碼內出現如下例代碼，存在多個if/else邏輯條件判斷式時，會有甚麼問題嗎?

```java
public class Transaction {
    public void doStuff(String option){
        if(option.equals("sell")){
            System.out.println("do complicated selling stuff...");
        } else if(option.equals("buy")){
            System.out.println("do complicated buying stuff...");
        } else if(option.equals("deposit")){
            System.out.println("do complicated depositing stuff...");
        }
    }
}
```

> [!DANGER|label:Porblems|iconVisibility:hidden]
> - 違反S.O.L.I.D.內的<font color='red'>**單一職責原則(Single of Responsibility)**</font> - method內做了太多事。
> - 違反S.O.L.I.D.內的<font color='red'>**開放封閉原則(Open–closed principle)**</font> -  一旦增減新的條件，也連帶影響其他條件式。
> - method難以閱讀與理解(實際產品代碼邏輯更為複雜)。
> - method<font color='red'>不易進行單元測試</font>，因為做的事情太多。
> - method不好除錯，易藏bug。
> - <font color='red'>重複代碼</font> - 易在其他地方發現重複對sell、buy與deposit的if/else條件式判斷代碼。
> - <font color='red'>Bad Smell - Long Method</font>

&nbsp;

## 可使用Enum來替換if/else
***
首先，新增一個enum，並定義sell, buy與deposit如下：
```java
public enum Action {
    SELL{
        @Override
        public void doSomeThing(){
            System.out.println("do complicated selling stuff...");
        }
    },
    BUY{
        @Override
        public void doSomeThing(){
            System.out.println("do complicated buying stuff...");
        }
    },
    DEPOSIT{
        @Override
        public void doSomeThing(){
            System.out.println("do complicated depositing stuff...");
        }
    };

    public abstract void doSomeThing();
}
```
而原先Transaction內便可移除if/else了，如下：
```java
public class Transaction {
    public void doStuff(Action action){
        action.doSomeThing();
    }
}
```
測試代碼如下：
```java
public static void main(String[] args) {
	new Transaction().doStuff(Action.BUY);
}
```

&nbsp;

## 哪些問題得已被解決：
> [!DANGER|label:Porblems|iconVisibility:hidden]
> - 違反S.O.L.I.D.內的<font color='red'>**單一職責原則(Single of Responsibility)**</font> - method內做了太多事。
>> 雖然Transaction的method確實解決了這件事，但這些職責被移到Action，而Action類別內仍做了太多事，另外產生了Large Class壞味道，而單一職責原則問題只是從method被移到class<sub>[註]</sub>。

> <br>

> - 違反S.O.L.I.D.內的<font color='red'>**開放封閉原則(Open–closed principle)**</font> -  一旦增減新的條件，也連帶影響其他條件式。
>> 雖然Transaction的method確實解決了這件事，但無可避免一旦未來對Action進行增減項目，開放封閉原則問題亦只是從method被移到class<sub>[註]。

> <br>

> - ~~method難以閱讀與理解(實際產品代碼邏輯更為複雜)。~~
>> method簡化成一行，而改用enum內實作方式將不同行為區隔成各enum物件內的method，可讀性相對提升。

> <br>

> - ~~method<font color='red'>不易進行單元測試</font>，因為做的事情太多。~~
>> method簡化成一行，測試邏輯移轉至Action。且由於改為Enum物件實作各自的行為，因此可針對不同enum type進行單元測試。

> <br>

> - ~~method不好除錯，易藏bug。~~
>> 邏輯由各enum物件管理，不像之前混在同一個method內，維護相對簡單。

> <br>

> - ~~<font color='red'>重複代碼</font> - 易在其他地方發現重複對sell、buy與deposit的if/else條件式判斷代碼。~~
>> 假設有多個地方均有原先對不同行為判斷的if/else，改為enum物件後，所有if/else均可移除。

> <br>

> - ~~<font color='red'>Bad Smell - Long Method</font>~~
>> method簡化成一行。

&nbsp;

## [註]
***
- 對於這類條件式型別碼，可考慮使用多型取代條件表示(**Replace Conditional with Polymorphism**)來取代。

- 可利用重構技巧的<font color='red'>Replace Type Code with Subclass</font>或<font color='red'>Replace Type Code with State/Strategy</font>來達到**Replace Conditional with Polymorphism**。